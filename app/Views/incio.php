<?php
include('app/global/sesiones.php');
?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Naruto | Inicio</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="public/css/inicio.css">
</head>

<body class="fadeIn">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="?controller=usuario&action=vistaInicio">Naruto</a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aldeas</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="?controller=aldea&action=vistaRegistro">Registrar</a>
                    <a class="dropdown-item" href="?controller=aldea&action=vistaConsultar">Busqueda</a>
                    <a class="dropdown-item" href="?controller=aldea&action=vistaModificar">Modificar</a>
                    <a class="dropdown-item" href="?controller=aldea&action=vistaEliminar">Eliminar</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Clanes</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="?controller=clan&action=vistaRegistro">Registrar</a>
                    <a class="dropdown-item" href="?controller=clan&action=vistaConsultar">Busqueda</a>
                    <a class="dropdown-item" href="?controller=clan&action=vistaModificar">Modificar</a>
                    <a class="dropdown-item" href="?controller=clan&action=vistaEliminar">Eliminar</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ninjas</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="?controller=ninja&action=vistaRegistro">Registrar</a>
                    <a class="dropdown-item" href="?controller=ninja&action=vistaConsultar">Busqueda</a>
                    <a class="dropdown-item" href="?controller=ninja&action=vistaModificar">Modificar</a>
                    <a class="dropdown-item" href="?controller=ninja&action=vistaEliminar">Eliminar</a>
                </div>
            </li>
            <li class="nav-item "><a class="nav-link" href="?controller=usuario&action=logout">Salir</a></li>
        </ul>
    </div>
</nav>

    <div class="container p-4">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <h1 class="h3 mb-3 font-weight-normal text-center">¡Bienvenido al camino Ninja!</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card card-body text-center card border-secondary mb-3" id="updateData">
                    <img src="public/imagenes/narutoNinja.png" class="card-img-top img-responsive" alt="Imagen Ninja">
                    <h5 class="card-title">Antes de comenzar...</h5>
                    <p class="card-text">Deberas registar algunos Ninjas para poder iniciar.<br>Si no te convence, podras modificar y/o eliminarlos en cualquier momento.</p>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</html>