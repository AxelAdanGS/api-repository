<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Naruto | Registro de Usuario</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" href="public/css/registrarAldeaClanesNinjas.css">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark center-block">
        <a class="navbar-brand" href="index.php">Naruto</a>
    </nav>

    <div class="container p-4 fadeIn">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <h1 class="h3 mb-3 font-weight-normal text-center">¡Registrate!</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card card-body">
                    <div class="container text-center">
                        <img class="mb-4 img-center" src="public/imagenes/ninja.jpg" alt="Usuario"width="200" height="200">
                    </div>
                    <form method="post" action="?controller=usuario&action=registrarUser">
                        <div class="form-group">
                            <input type="text" name="id" id="id" class="form-control" placeholder="Asigna un ID a tu Usuario" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="nombre" id="nombre" class="form-control" placeholder="Nombre(s)" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="apPaterno" id="apPaterno" class="form-control" placeholder="Apellido Paterno" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="apMaterno" id="apMaterno" class="form-control" placeholder="Apellido Materno" required>
                        </div>
                        <div class="form-group">
                            <input type="number" name="edad" id="edad" class="form-control" placeholder="Edad" required value="18" min="18" max="99">
                        </div>
                        <div class="form-group">
                            <input type="email" name="correo" id="correo" class="form-control" placeholder="Correo Electrónico" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
                        </div>
                        <input type="submit" class="btn btn-success btn-block fadeIn fourth" name="register" value="Registrarme">
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
</body>
</html>