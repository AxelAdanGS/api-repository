<?php
    
    use Models\Conexion;
    use Models\Ninja;
    //Incluimos las clases a utilizar
    include "app/Models/Conexion.php";
    include "app/Models/Ninja.php";

    //Clase ninja controller
    class ninjaController{

        /*
         * VISTAS
         */
        //Vista registro
        public function vistaRegistro(){
            require 'app/Views/registrarNinja.php';
        }
        //Funcion para vista de modificar datos
        public function vistaModificar(){
            require 'app/Views/actualizarNinja.php';
        }
        //Funcion para vista de eliminar
        public function vistaEliminar(){
            require 'app/Views/eliminarNinja.php';
        }
        //Funcion para vista de consultar
        public function vistaConsultar(){
            require 'app/Views/busquedaNinja.php';
        }
        /*
        * REGISTRO DE NINJAS
        */
        //Funcion para registrar datos
        public function crearNinja(){
            //Comprueba que se hayan mandado valores
            if(isset($_POST)) {

                //Se instancia el objeto de productos para utilizar sus atributos
                $ninja = new \Models\Ninja();

                //Accedemos a sus atributos y les damos valores por metodo POST
                $ninja->id = $_POST['id'];
                $ninja->nombre = $_POST['nombre'];
                $ninja->clan = $_POST['clan'];
                $ninja->edad = $_POST['edad'];
                $ninja->genero = $_POST['genero'];
                $ninja->estadoVida = $_POST['estadoV'];
                $ninja->aldea = $_POST['aldea'];
                $ninja->madre = $_POST['nombreP'];
                $ninja->padre = $_POST['nombreM'];
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status" => "succes insertarNinja", "Ninja" => $ninja->insertarNinja()]);
                echo '<br><a href="?controller=ninja&action=vistaRegistro">Regresar</a>';

            }

        }
        /*
        * CONSULTA DE NINJAS
        */
        //Funcion para consultar datos
        public function consultarNinja(){

            $opciones = $_POST['opciones'];
            if($opciones !== ""){
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarNinja","ninjas"=> $ninja = Ninja::imprimirNinja($opciones)]);
            }
            else{
                 //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarNinja","ninjas"=> $ninja = Ninja::imprimirNinjas()]);
            }
            echo '<br><a href="?controller=ninja&action=vistaConsultar">Regresar</a>';

        }
        /*
        * MODIFICAR NINJAS
        */
        //Funcion para modificar datos
        public function modificarNinja(){
            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $ninja = new \Models\Ninja();

                $id = $_POST['id'];
                $nombre = $_POST['nombre'];
                $clan = $_POST['clan'];
                $edad = $_POST['edad'];
                $genero = $_POST['genero'];
                $estadoVida = $_POST['estadoV'];
                $aldea = $_POST['aldea'];
                $madre = $_POST['nombreP'];
                $padre = $_POST['nombreM'];
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status" => "succes modificarNinja", "Ninja" => $ninja->actualizarNinja($id, $nombre,$clan, $edad, $genero, $estadoVida,$aldea, $padre, $madre)]);
                echo '<br><a href="?controller=ninja&action=vistaModificar">Regresar</a>';

            }
        }
        /*
        * ELIMINAR NINJAS
        */
        //Funcion para eliminar datos
        public function eliminarNinja(){
            //Mandamos la vista de eliminar

            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $ninja = new \Models\Ninja();

                //Accedemos a sus atributos y les damos valores por metodo POST
                $id = $_POST['id'];

                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes eliminarNinja","Ninja"=> $ninja->eliminar($id)]);
                echo '<br><a href="?controller=ninja&action=vistaEliminar">Regresar</a>';

            }
        }
    }
?>