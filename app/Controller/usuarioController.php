<?php

    use Models\Usuario;
//Incluimos las clases a utilizar
include "app/Models/Conexion.php";
include "app/Models/Usuario.php";

class usuarioController
{
    /*
     * VISTA REGISTRO
     */
    function vistaRegistro(){
        //Mandamos la vista del formulario del registro
        require 'app/Views/registrarUsuario.php';

    }
    //vista login
    function vistaLogin(){
        //Mandamos la vista del formulario del login
        require 'app/Views/login.php';
    }
    function  vistaInicio(){
        require 'app/Views/incio.php';
    }
     /*
     * REGISTRO DE USUARIO
     */
    //Funcion para registrar datos
    public function registrarUser(){
        if(isset($_POST)){
            //Mandamos la vista del registro
            //require 'app/Views/registrarUsuario.php';

            //Creamos un objeto usuario
            $user = new \Models\Usuario();

            //Accedemos a los atirbutos de usuario y mandamos valores con el formulario
            $user->idUsuario = $_POST['id'];
            $user->nombre = $_POST['nombre'];
            $user->apPaterno = $_POST['apPaterno'];
            $user->apMaterno = $_POST['apMaterno'];
            $user->edad = $_POST['edad'];
            $user->correo = $_POST['correo'];
            $user->contrasenia = $_POST['password'];
            
            //Imprimimos que la operacion fue realizada correctamente
            echo json_encode(["status"=>"succes registrarU","usuario"=> $user->insertarUser()]);
            echo '<br><a href="?controller=usuario&&action=login">Regresar</a>';
        }
    }
    function login(){
        if(isset($_POST['correo']) && isset($_POST['password']) ){
            //CREAMOS NUEVO OBJETO DE USUARIO
            $user = new \Models\Usuario();

            //Recogemos variables
            $correo = $_POST['correo'];
            $password = $_POST['password'];

            //Llama al metodo con parametros
            $verificar = Usuario::logins($correo, $password);

            //Verificar parametros
            if(!$verificar){
                echo "Datos erroneos";
                echo '<br><a href="index.php">Regresar</a>';
            }
            else{
                session_start();
                //Guarda los datos del usuariio logeado
                $_SESSION['usuario'] = $verificar;

                //Guarda el id del usuario logeado
                $_SESSION['idUsuario'] = $verificar->ID_Usuario;

                echo "Bienvenido: " . $verificar->Nombre;
                echo '<br><a href="?controller=usuario&action=vistaInicio">ENTRAR</a>';
            }

        }
        else {
            require 'app/Views/login.php';

        }

    }
    //funcion para cerrar sesion
    function logout()
    {
        session_start();
        session_destroy();
        require 'app/Views/login.php';
    }
    /*
     * PRUEBA DE $_SESSION, SIRVEPARA DETEERINAR VAARIABLES INICIADOS O QUE DEBEN DE ESTAR
    public function prueba(){
        $_SESSION['var1'] = "hola";
        echo "Ya creaste";
        var_dump($_SESSION['var1']);
        unset($_SESSION['var1']);
        echo "Ya fue";
        var_dump($_SESSION['var1']);

    }
    public function mostrarPrueba(){
        var_dump($_SESSION['var1']);
    }
    */

}
?>