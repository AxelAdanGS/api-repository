<?php
    
    use Models\Conexion;
    use Models\Clan;
    //Incluimos las clases a utilizar
    include "app/Models/Conexion.php";
    include "app/Models/Clan.php";

    //Clase clan controller
    class clanController{
        /*
         * VISTAS
         */
        //Vista registro
        public function vistaRegistro(){
            require 'app/Views/registrarClan.php';
        }
        //Funcion para vista de modificar datos
        public function vistaModificar(){
            require 'app/Views/actualizarClan.php';
        }
        //Funcion para vista de eliminar
        public function vistaEliminar(){
            require 'app/Views/eliminarClan.php';
        }
        //Funcion para vista de consultar
        public function vistaConsultar(){
            require 'app/Views/busquedaClan.php';
        }

        /*
        * REGISTRO DE CLANES
        */
        //Funcion para registrar datos
        public function crearClan(){

            //Comprueba que se hayan mandado valores
            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $clan = new \Models\Clan();

                //Accedemos a sus atributos y les damos valores por metodo POST
                $clan->id = $_POST['id'];
                $clan->nombre = $_POST['nombre'];
                $clan->aldea = $_POST['aldea'];
                $clan->elemento = $_POST['elemento'];
                $clan->caracteristica = $_POST['caracteristica'];

                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes insertar","clan"=> $clan->registrar()]);
                echo '<br><a href="?controller=clan&action=vistaRegistro">Regresar</a>';

            }
        }
        /*
        * CONSULTA DE CLANES
        */
        //Funcion para consultar datos
        public function consultarClan(){
            $opciones = $_POST['opciones'];
            if($opciones !== ""){
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarClan","clan"=> $clan = Clan::imprimirClan($opciones)]);
            }
            else{
                 //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarClanes","clanes"=> $clan = Clan::imprimirClanes()]);
                
            }
            echo '<br><a href="?controller=clan&action=vistaConsultar">Regresar</a>';

        }
        /*
        * MODIFICAR CLANES
        */
        //Funcion para modificar datos
        public function modificarClan(){
            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $clan = new \Models\Clan();

                //Accedemos a sus atributos y les damos valores por metodo POST
                $id = $_POST['id'];
                $nombre = $_POST['nombre'];
                $aldea = $_POST['aldea'];
                $elemento = $_POST['elemento'];
                $caracteristica = $_POST['caracteristica'];

                //Imprimimos qe se realizzo correecamenttee
                echo json_encode(["status"=>"succes actualizarClan","Clan"=>$clan->actualizarClan($id, $nombre, $aldea, $elemento, $caracteristica)]);
                echo '<br><a href="?controller=clan&action=vistaModificar">Regresar</a>';

            }
        }
        /*
        * ELIMINAR CLANES
        */
        //Funcion para eliminar datos
        public function eliminarClan(){

            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $clan = new \Models\Clan();

                //Accedemos a sus atributos y les damos valores por metodo POST
                $id = $_POST['id'];

                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes eliminarClan","Clan"=> $clan->eliminar($id)]);
                echo '<br><a href="?controller=clan&action=vistaEliminar">Regresar</a>';

            }

        }
    }
?>