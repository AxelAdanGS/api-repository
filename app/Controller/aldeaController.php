<?php
    
    use Models\Aldea;
    
    //Incluimos las clases a utilizar
    include "app/Models/Conexion.php";
    include "app/Models/Aldea.php";

    //Clase aldea controller
    class aldeaController{
    
        /*
        * REGISTRO DE ALDEAS
        */
        //Funcion para vista de registrar datos
        public function vistaRegistro(){
            require 'app/Views/registrarAldea.php';
        }
        //Funcion para vista de modificar datos
        public function vistaModificar(){
            require 'app/Views/actualizarAldea.php';
        }
        //Funcion para vista de eliminar
        public function vistaEliminar(){
            require 'app/Views/eliminarAldea.php';
        }
        //Funcion para vista de consultar
        public function vistaConsultar(){
            require 'app/Views/busquedaAldea.php';
        }

        //Funcion para insertar
        function insertar(){

            //Comprueba que se hayan mandado valores
            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $aldea = new \Models\Aldea();
                
                //Accedemos a sus atributos y les damos valores por metodo POST
                $aldea->id = $_POST['id'];
                $aldea->nombre = $_POST['nombre'];
                $aldea->sobrenombre = $_POST['sobreN'];
                $aldea->pais = $_POST['pais'];
                $aldea->fundador = $_POST['fundador'];
                $aldea->kage = $_POST['kage'];

                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes insertar","aldea"=> $aldea->registrar()]);
                echo '<br><a href="?controller=aldea&action=vistaRegistro">Regresar</a>';

            }
            //vistaRegistro();
        }

        /*
        * CONSULTA DE ALDEAS
        */
        //Funcion para consultar datos
        //funcion para mostrar ESTATICA
        //Funcion para mostrar todos los proveedores
        public function mostrarAldeas(){
            $opciones = $_POST['opciones'];
            if($opciones !== ""){
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarAldeas","aldeas"=> $ald = Aldea::imprimirAldea($opciones)]);
            }
            else{
                 //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes mostrarAldeas","aldeas"=> $ald = Aldea::imprimirAldeas()]);
            }
            echo '<br><a href="?controller=aldea&action=vistaConsultar">Regresar</a>';
        }

        /*
        * MODIFICAR ALDEAS
        */
        //Funcion para modificar datos
        public function modificarAldea(){
            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $aldea = new \Models\Aldea();
                
                //Accedemos a sus atributos y les damos valores por metodo POST
                $id = $_POST['id'];
                $nombre = $_POST['nombre'];
                $sobrenombre = $_POST['sobreN'];
                $pais = $_POST['pais'];
                $fundador = $_POST['fundador'];
                $kage = $_POST['kage'];
                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes actualizarAldeas","Aldeas"=>$aldea->actualizarAldea($id, $nombre, $sobrenombre, $pais, $fundador, $kage)]);
                echo '<br><a href="?controller=aldea&action=vistaModificar">Regresar</a>';
            }
        }
        /*
        * ELIMINAR ALDEAS
        */
        //Funcion para eliminar datos
        public function eliminarAldea(){

            //Mandamos la vista de eliminar

            if(isset($_POST)){

                //Se instancia el objeto de productos para utilizar sus atributos
                $aldea = new \Models\Aldea();
                
                //Accedemos a sus atributos y les damos valores por metodo POST
                $id = $_POST['id'];

                //Imprimimos que la operacion fue realizada correctamente
                echo json_encode(["status"=>"succes eliminarAldea","Aldea"=>  $aldea->eliminar($id)]);
                echo '<br><a href="?controller=aldea&action=vistaEliminar">Regresar</a>';

            }
        }
    }
?>