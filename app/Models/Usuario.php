<?php

namespace Models;

class Usuario extends Conexion
{
    public $idUsuario;
    public $nombre;
    public $apPaterno;
    public $apMaterno;
    public $edad;
    public $correo;
    public $contrasenia;

    //funcion insertar
    public function insertarUser()
    {
        //Prepara la sentencia sql
        $pre = mysqli_prepare($this->con, "INSERT INTO usuarios(ID_Usuario, Nombre, Apellido_Paterno, Apellido_Materno, Edad, Correo, Contrasenia) VALUES (?,?,?,?,?,?,?)");
        //Pasa parametros con las variables y sus tipos
        $pre->bind_param("isssiss", $this->idUsuario, $this->nombre, $this->apPaterno, $this->apMaterno, $this->edad, $this->correo, $this->contrasenia);
        //Ejecuta el query
        $pre->execute();
    }

     static function logins($correo, $password){
        //Preparamos la conexion instanciandola
        $co = new \Models\conexion();

        //preparamos la sentencia sql
        $pre = mysqli_prepare($co->con, "SELECT * FROM usuarios WHERE Correo = ? AND Contrasenia = ?");

        $pre->bind_param("ss", $correo, $password);
        //Ejecutamos el query
        $pre->execute();

        //Almacenamos el resultado obtenido del objeto
        $resul = $pre->get_result();

        return $resul->fetch_object();
    }
/*

}
/*
//funcion insertar
function insertarProveedor(){

    //preparamos la sentencia sql
    $pre = mysqli_prepare($this->con, "INSERT INTO proveedores (Id,Nombre, Cp, Calle, Municipio, Estado, Pais, Telefono) VALUES (?,?,?,?,?,?,?,?)");

    //Pasamos valores
    $pre->bind_param("isisssss",  $this->idProveedor,$this->nombreProveedor, $this->cpProveedor, $this->calleProveedor, $this->municipioProveedor, $this->estadoProveedor, $this->paisProveedor, $this->telProveedor);

    //Ejecutamos query
    $pre->execute();

    return true;
}

//funcion para mostrar ESTATICA
static function imprimirProveedores(){

    //Preparamos la conexion instanciandola
    $co = new \Models\conexion();

    //preparamos la sentencia sql
    $pre = mysqli_prepare($co->con, "SELECT * FROM proveedores");

    //Ejecutamos el query
    $pre->execute();

    //Almacenamos el resultado obtenido del objeto
    $resul = $pre->get_result();

    //Arreglo para guardar cada dato de cada campo
    $prov = array();

    //Ciclo para recorrer los objetos del resultado
    while($proveedor = $resul->fetch_object(proveedores::class)){

        //Agregamos cada elemento al arreglo
        array_push($prov, $proveedor);
    }

    //retornamos el arreglo con los datos
    return $prov;
}

//Funcion para mostrar por ID ESTATICA con parametros
static function imprimirProveedor($id){

    //Preparamos la conexion instanciandola
    $co = new \Models\conexion();

    //preparamos la sentencia sql
    $pre = mysqli_prepare($co->con, "SELECT * FROM proveedores WHERE Id = ?");

    //Pasamos valores
    $pre->bind_param("i",  $id);

    //Ejecutamos query
    $pre->execute();

    //Almacenamos el resultado obtenido
    $resul = $pre->get_result();

    //Retornamos el resultado del objeto
    return $resul->fetch_object();
}

//Funcion para actualizar datos (nombre) por ID
function actualizarProv($id, $nombre){

    //preparamos la sentencia sql
    $pre = mysqli_prepare($this->con, "UPDATE proveedores SET Nombre = ? WHERE Id = ?");

    //Pasamos valores
    $pre->bind_param("si",   $nombre, $id);

    //Ejecutamos query
    $pre->execute();

    //Retornamos que ha sido actualizado
    return true;
}

//Funcion para eliminar por ID
function eliminarProv($id){

    //preparamos la sentencia sql
    $pre = mysqli_prepare($this->con, "DELETE FROM proveedores WHERE Id = ?");

    //Pasamos valores
    $pre->bind_param("i",   $id);

    //Ejecutamos query
    $pre->execute();

    //Retornamos que ha sido eliminado
    return true;
}

}
*/
}