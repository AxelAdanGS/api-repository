<?php
    
    namespace Models;

    //Clase clan con herrencia de conexion
    class Clan extends Conexion{
        //Atributos de la clase
        public $id;
        public $nombre;
        public $aldea;
        public $elemento;
        public $caracteristica;


        //funcion insertar
        public function registrar(){

            //Prepara la sentencia sql
            $pre = mysqli_prepare($this->con,"INSERT INTO `clanes`(`ID_Clan`, `Nombre_Clan`, `Aldea`, `Elemento`, `Caracterisitica`) VALUES (?,?,?,?,?)");
            //Pasa parametros con las variables y sus tipos
            $pre->bind_param("isiss",  $this->id, $this->nombre, $this->aldea, $this->elemento, $this->caracteristica);

            //Ejecuta el query
            $pre->execute();

        }
        //Funcion imprimir aldeas
        static function imprimirClanes(){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM clanes");

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

            //Almacena en n arreglo los datos obtenidos
            while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            return $elementos;
        }
        //Funcion para actualizar datos (nombre) por ID
        function actualizarClan($id, $nombre, $aldea, $elemento, $caracteristica){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "UPDATE clanes SET Nombre_Clan= ?,Aldea= ?,Elemento= ?,Caracterisitica = ? WHERE ID_Clan= ?");

            //Pasamos valores
            $pre->bind_param("sissi", $nombre, $aldea, $elemento, $caracteristica, $id);

            //Ejecutamos query
            $pre->execute();

            //Retornamos que ha sido actualizado
            return true;
        }
        //Funcion para eliminar por ID
        function eliminar($id){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "DELETE FROM clanes WHERE ID_Clan= ?");

            //Pasamos valores
            $pre->bind_param("i", $id);

            //Ejecutamos query
            $pre->execute();

            //Retornamos que ha sido eliminado
            return true;
        }
        //Imprimir por ID
        static function imprimirClan($opciones){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM clanes WHERE ID_Clan = ? OR  Nombre_Clan= ? OR Aldea= ? OR Elemento= ? OR Caracterisitica = ?");
            
            //Pasamos valores
            $pre->bind_param("isiss", $opciones, $opciones, $opciones, $opciones, $opciones);

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

             //Almacena en n arreglo los datos obtenidos
            while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            if($elementos == ""){
                echo "No hay elementos";
            }

            else {
                return $elementos;
            }
        }
    }
?>