<?php
    
    namespace Models;

    //Clase ninja realizando conexion a la base de datos
    class Ninja extends Conexion{

        //Atributos
        public $id;
        public $nombre;
        public $clan;
        public $edad;
        public $genero;
        public $estadoVida;
        public $aldea;
        public $madre;
        public $padre;

        //funcion insertar
        public function insertarNinja()
        {
            //Prepara la sentencia sql
            $pre = mysqli_prepare($this->con, "INSERT INTO `ninjas`(`ID_Ninja`, `Nombre_Ninja`, `Clan`, `Edad`, `Genero`, `Estado_Vida`, `Aldea`, `Madre`, `Padre`) VALUES (?,?,?,?,?,?,?,?,?)");
            //Pasa parametros con las variables y sus tipos
            $pre->bind_param("isiisiiss", $this->id, $this->nombre, $this->clan, $this->edad, $this->genero, $this->estadoVida, $this->aldea, $this->madre, $this->padre);
            //Ejecuta el query
            $pre->execute();
        }
        //Funcion para actualizar datos (nombre) por ID
        function actualizarNinja($id, $nombre, $clan, $edad, $genero, $estadoV, $aldea, $padre, $madre){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "UPDATE `ninjas` SET `Nombre_Ninja`=?,`Clan`=?,`Edad`=?,`Genero`=?,`Estado_Vida`=?,`Aldea`=?,`Madre`=?,`Padre`=? WHERE `ID_Ninja`=?");

            //Pasamos valores
            $pre->bind_param("siisiissi", $nombre, $clan, $edad, $genero, $estadoV, $aldea, $padre, $madre, $id);

            //Ejecutamos query
            $pre->execute();

            //Retornamos que ha sido actualizado
            return true;
        }

        static function imprimirNinjas(){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM ninjas");

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

            //Almacena en n arreglo los datos obtenidos
            while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            return $elementos;
        }
        //Funcion para eliminar por ID
        function eliminar($id){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "DELETE FROM ninjas WHERE ID_Ninja = ?");

            //Pasamos valores
            $pre->bind_param("i", $id);

            //Ejecutamos query
            $pre->execute();

            //Retornamos que ha sido eliminado
            return true;
        }
        //Imprimir por ID
        static function imprimirNinja($opciones){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM ninjas WHERE `ID_Ninja`=? OR `Nombre_Ninja`=? OR `Clan`=? OR`Edad`=? OR `Genero`=? OR `Estado_Vida`=? OR `Aldea`=? OR `Madre`=? OR `Padre`=?");
            
            //Pasamos valores
            $pre->bind_param("isiisiiss", $opciones, $opciones, $opciones, $opciones, $opciones, $opciones, $opciones, $opciones, $opciones);

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

            //Almacena en n arreglo los datos obtenidos
            while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            if($elementos == ""){
                echo "No hay elementos";
            }

            else {
                return $elementos;
            }
        }
    }
?>