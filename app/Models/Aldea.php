<?php
    
    namespace Models;

    //Clase aldea heredada de conexion
    class Aldea extends Conexion{

        //Atriutos de la clase
        public $id;
        public $nombre;
        public $sobrenombre;
        public $pais;
        public $fundador;
        public $kage;

        //funcion insertar
        public function registrar(){

            //Prepara la sentencia sql
            $pre = mysqli_prepare($this->con,"INSERT INTO aldeas(ID_Aldea, Nombre_Aldea, SobreNombre, Pais, Fundador, Kage) VALUES (?,?,?,?,?,?)");
            //Pasa parametros con las variables y sus tipos
            $pre->bind_param("isssss",  $this->id, $this->nombre, $this->sobrenombre, $this->pais, $this->fundador, $this->kage);

            //Ejecuta el query
            $pre->execute();

        }
         //Funcion para actualizar datos (nombre) por ID
         function actualizarAldea($id, $nombre, $sobreN, $pais, $fundador,$kage){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "UPDATE aldeas SET Nombre_Aldea = ?, SobreNombre = ?, Pais = ?, Fundador = ?, Kage = ?  WHERE ID_Aldea = ?");
    
            //Pasamos valores
            $pre->bind_param("sssssi", $nombre, $sobreN, $pais, $fundador, $kage, $id);
    
            //Ejecutamos query
            $pre->execute();
    
            //Retornamos que ha sido actualizado
            return true;
        }
        //Funcion para eliminar por ID
        function eliminar($id){

            //preparamos la sentencia sql
            $pre = mysqli_prepare($this->con, "DELETE FROM aldeas WHERE ID_Aldea = ?");

            //Pasamos valores
            $pre->bind_param("i", $id);

            //Ejecutamos query
            $pre->execute();

            //Retornamos que ha sido eliminado
            return true;
        }
        static function imprimirAldeas(){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM aldeas");

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

            //Almacena en n arreglo los datos obtenidos
            while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            return $elementos;
        }
        static function imprimirAldea($opciones){
            //Preparamos la conexion instanciandola
            $co = new \Models\conexion();

            //preparamos la sentencia sql
            $pre = mysqli_prepare($co->con, "SELECT * FROM aldeas WHERE ID_Aldea = ? OR Nombre_Aldea = ? OR SobreNombre = ? OR Pais = ? OR Fundador = ? OR Kage = ?");
            
            //Pasamos valores
            $pre->bind_param("isssss", $opciones, $opciones, $opciones, $opciones, $opciones, $opciones);

            //Ejecutamos el query
            $pre->execute();

            //Almacenamos el resultado obtenido del objeto
            $resul = $pre->get_result();

           //Almacena en n arreglo los datos obtenidos
           while($elemento = mysqli_fetch_assoc($resul)){
                $elementos[] = $elemento;
            }
            if($elementos == ""){
                echo "No hay elementos";
            }

            else {
                return $elementos;
            }
        }
    }
?>