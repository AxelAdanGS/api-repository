<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
</head>
<body>

    <?php
        use Models\Aldea;
        use Models\Clan;
        use Models\Ninja;
        //Comprueba que se le pase un controlador y una accion
        if(isset($_GET["controller"]) && isset($_GET["action"])){

            //Asigna el valor ingresado de cada metodo
            $controller = $_GET["controller"]."Controller";
            $action = $_GET["action"];

            //Llama a la clase a utilizar concatenando "controller"
        

            //
            require "app/Controller/$controller.php";

            //require_once("Controller/" .$controller. ".php");

            $objeto = new $controller();

            $objeto->{$action}();
        }
        else{
            require "app/Views/login.php";
        }
    ?>
</body>
</html>